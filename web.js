// JavaScript
const form = document.getElementById('myForm');
const nameInput = document.getElementById('name');
const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('password');

form.addEventListener('submit', (e) => {
    e.preventDefault();
    validateForm();
});

function validateForm() {
    let nameError = '';
    let emailError = '';
    let passwordError = '';

    if (nameInput.value.length < 5) {
        nameError = ' Panjang nama minimal harus 5 karakter.';
    }

    if (!validateEmail(emailInput.value)) {
        emailError = 'email tidak valid.';
    }

    if (passwordInput.value.length < 8) {
        passwordError = 'kata sandi minimal harus 8 karakter.';
    }

    if (nameError || emailError || passwordError) {
        alert(`Error: ${nameError} ${emailError} ${passwordError}`);
    } else {
        alert('Form validasi berhasil!');
    }
}

function validateEmail(email) {
    const re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return re.test(email);
}